import { Provide, Get, Post, Validate, Body, ALL } from '@midwayjs/decorator';
import { CoolController, BaseController } from 'midwayjs-cool-core';
import { ArticleDTO } from '../../../dto/article';
import { AppArticleEntity } from '../../../entity/article/article'

/**
 * 描述
 */
@Provide()
@CoolController({
  prefix: '/admin/article',
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: AppArticleEntity,
})
export class AppArticleController extends BaseController {
  @Post('/add')
  @Validate()
  async add(@Body(ALL) article: ArticleDTO) {
    
  }
  /**
    * 其他接口
    */
  @Get('/other')
  async other() {
    return this.ok('hello, this msg is send by zxq!!!');
  }
}
