import { Rule, RuleType } from "@midwayjs/decorator";

export class ArticleDTO {
  
  @Rule(RuleType.required())
  title: string;
}