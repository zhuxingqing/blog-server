import { EntityModel } from '@midwayjs/orm';
import { BaseEntity } from 'midwayjs-cool-core';
import { Column } from 'typeorm';

/**
 * 描述
 */
@EntityModel('app_article')
export class AppArticleEntity extends BaseEntity {
  @Column({ comment: '标题' })
  title: string;

  @Column({ comment: '封面图片' })
  pic: string;

  @Column({ comment: '概要' })
  outline: string;

  @Column({ comment: '作者' })
  author: string;

  @Column({ comment: '内容' })
  content: string;
}
