import { Inject, Provide } from '@midwayjs/decorator';
import { BaseService } from 'midwayjs-cool-core';
import { InjectEntityModel } from '@midwayjs/orm';
import { Repository } from 'typeorm';
import { AppArticleEntity } from '../../entity/article/article';
import { Context } from 'egg';

/**
 * 描述
 */
@Provide()
export class AppArticleService extends BaseService {
  @Inject()
  ctx: Context;

  @InjectEntityModel(AppArticleEntity)
  appArticleEntity: Repository<AppArticleEntity>;

  /**
   * 描述
   */
  async xxx() {}
}
